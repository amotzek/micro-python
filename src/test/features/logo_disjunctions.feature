Feature: Use disjunctions

  Background:
    Given I start with an empty page

  Scenario: True or false
    When I execute the logo commands "pendown; if 1 < 2 or 2 < 1 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Short circuit
    When I execute the logo commands "pendown; if 1 < 2 or 20 / 0 < 1 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: True or true
    When I execute the logo commands "pendown; if 1 < 2 or 1 < 2 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: False or true
    When I execute the logo commands "pendown; if 2 < 1 or 1 < 2 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: False or false
    When I execute the logo commands "pendown; if 2 < 1 or 2 < 1 [ forward 10 ]"
    Then the page must have 0 lines

  Scenario: False or false or true
    When I execute the logo commands "pendown; if 2 < 1 or 2 < 1 or 2 - 1 < 1 + 2 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

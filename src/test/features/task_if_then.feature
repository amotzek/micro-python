Feature: Execute tasks with conditions

    Background:
        Given I have no tasks
          And I have no flags

    Scenario: Execute no task
        Given I clear the flag "f"
         When I start the task "a" when the flag "f" is set
          And I execute at most one task
         Then I must have one or more unfinished tasks
          And I must not have any finished task

    Scenario: Execute one task
        Given I set the flag "f"
         When I start the task "a" when the flag "f" is set
          And I execute at most one task
         Then I must have finished 1 tasks
          And I must have finished the task "a"

    Scenario: Execute one task after setting the flag
        Given I clear the flag "f"
         When I start the task "a" when the flag "f" is set
          And I execute at most one task
         Then I must have one or more unfinished tasks
          And I must not have any finished task
        Given I set the flag "f"
         When I execute all tasks
         Then I must have finished 1 tasks
          And I must have finished the task "a"

    Scenario: Execute one task after a delay and then another task after setting the flag
        Given I clear the flag "f"
          And I start the clock
         When I start the task "a" when the flag "f" is set
          And I start the task "b" after 30 milliseconds
          And I execute at most one task
          And I execute at most one task
         Then I must have finished 1 tasks
          And I must have finished the task "b"
          And I must have one or more unfinished tasks
          And at least 30 milliseconds are elapsed
        Given I set the flag "f"
         When I execute all tasks
         Then I must have finished 2 tasks
          And I must have finished the task "b" at position 0
          And I must have finished the task "a" at position 1

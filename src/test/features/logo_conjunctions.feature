Feature: Use conjunctions

  Background:
    Given I start with an empty page

  Scenario: True and false
    When I execute the logo commands "pendown; if 1 < 2 [ if 2 < 1 [ forward 10 ]]"
    Then the page must have 0 lines

  Scenario: True and true
    When I execute the logo commands "pendown; if 1 < 2 [ if 1 < 2 [ forward 10 ]]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: False and true
    When I execute the logo commands "pendown; if 2 < 1 [ if 1 < 2 [ forward 10 ]]"
    Then the page must have 0 lines

  Scenario: False and false
    When I execute the logo commands "pendown; if 2 < 1 [ if 2 < 1 [ forward 10 ]]"
    Then the page must have 0 lines

Feature: Use procedures

  Background:
    Given I start with an empty page

  Scenario: Draw two squares
    When I execute the logo commands "to square :side [ repeat 4 [ forward :side; right 90 ] ] pendown; square 15; penup; setxy 5 -5; pendown; square 5"
    Then the page must have 8 lines
     And the page must have a line from (0, 0) to (15, 0)
     And the page must have a line from (15, 0) to (15, -15)
     And the page must have a line from (15, -15) to (0, -15)
     And the page must have a line from (0, -15) to (0, 0)
     And the page must have a line from (5, -5) to (10, -5)
     And the page must have a line from (10, -5) to (10, -10)
     And the page must have a line from (10, -10) to (5, -10)
     And the page must have a line from (5, -10) to (5, -5)

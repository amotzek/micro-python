from behave import *
from cooperative_multitasking import Tasks

@given('I have no tasks')
def no_tasks(context):
    context.tasks = Tasks()
    context.task_history = []

@when('I start the task "{name}" with priority {prio:n} now')
def start_task_now(context, name, prio):
    context.tasks.now(lambda : context.task_history.append(name), priority = prio)

@when('I start the task "{name}" now')
def start_task_now(context, name):
    context.tasks.now(lambda : context.task_history.append(name))

@when('I start the task "{name}" after {duration:n} milliseconds')
def start_task_after(context, name, duration):
    context.tasks.after(duration, lambda : context.task_history.append(name))

@when('I start the task "{task_name}" when the flag "{flag_name}" is set')
def start_task_when_flag(context, task_name, flag_name):
    context.tasks.when_then(lambda : context.flags[flag_name], lambda : context.task_history.append(task_name))

@when('I execute all tasks')
def execute_all_tasks(context):
    while context.tasks.available():
        context.tasks.run()

@when('I execute at most one task')
def execute_at_most_one_task(context):
    context.tasks.run()

@then('I must have one or more unfinished tasks')
def has_tasks(context):
    assert context.tasks.available(), 'there should be a task'

@then('I must not have any finished task')
def no_finished_task(context):
    assert len(context.task_history) == 0, f'list {context.task_history} should be empty'

@then('I must have finished {count:n} tasks')
def count_finished_tasks(context, count):
   assert len(context.task_history) == count, f'list {context.task_history} should have {count} elements'

@then('I must have finished the task "{name}"')
def task_finished(context, name):
    assert name in context.task_history, f'list {context.task_history} should contain {name}'

@then('I must have finished the task "{name}" at position {pos:n}')
def task_finished_at_position(context, name, pos):
    assert name == context.task_history[pos], f'list {context.task_history} should have {name} at position {pos}'

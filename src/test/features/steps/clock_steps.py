from behave import *
import time

@given('I start the clock')
def start_clock(context):
    context.start_time = time.monotonic()

@then('at least {duration:n} milliseconds are elapsed')
def elapsed_time(context, duration):
    elapsed_time = 1000.0 * (time.monotonic() - context.start_time) # in Millisekunden
    assert elapsed_time >= duration - 16.0, f'only {elapsed_time} milliseconds are elapsed, should be more than {duration}'

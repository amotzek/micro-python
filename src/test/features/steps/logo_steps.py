from behave import *
from turtle import Turtle
from logo import Logo

def count_lines(turtle):
    colored_lines = turtle.get_colored_lines()
    count = 0
    for colored_line in colored_lines:
        line = colored_line[0]
        count += len(line)
        count -= 1
    return count

def contains_line(x_src, y_src, x_dest, y_dest, turtle):
    colored_lines = turtle.get_colored_lines()
    for colored_line in colored_lines:
        line = colored_line[0]
        for i in range(len(line) - 1):
            if line[i] == (x_src, y_src) and line[i + 1] == (x_dest, y_dest):
                return True
    return False

@given('I start with an empty page')
def empty_page(context):
    context.turtle = Turtle()
    context.logo = Logo(context.turtle)

@when('I execute the logo commands "{commands}"')
def execute_commands(context, commands):
    context.logo.execute(commands)

@then('the page must have {count:n} lines')
def line_count(context, count):
    if count != count_lines(context.turtle):
        raise ValueError('page should have ' + str(count) + ' lines, but there were ' + str(len(context.turtle.lines)))

@then('the page must have a line from ({x_src:n}, {y_src:n}) to ({x_dest:n}, {y_dest:n})')
def made_line(context, x_src, y_src, x_dest, y_dest):
    if not contains_line(x_src, y_src, x_dest, y_dest, context.turtle):
        raise ValueError('did not find line from (' + str(x_src) + ', ' + str(y_src) + ') to (' + str(x_dest) + ', ' + str(y_dest) + ') in ' + str(context.turtle.colored_lines))

@then('the page must have a width of {w_expected:n}')
def check_width(context, w_expected):
    w_actual = context.turtle.get_bounds()[2]
    if w_expected != w_actual:
        raise ValueError('expected witdh ' + str(w_expected) + ', but was ' + str(w_actual))

@then('the page must have a height of {h_expected:n}')
def check_height(context, h_expected):
    h_actual = context.turtle.get_bounds()[3]
    if h_expected != h_actual:
        raise ValueError('expected height ' + str(h_expected) + ', but was ' + str(h_actual))

@then('the page must have an origin of ({x_expected:n}, {y_expected:n})')
def check_origin(context, x_expected, y_expected):
    x_actual = context.turtle.get_bounds()[0]
    y_actual = context.turtle.get_bounds()[1]
    if x_expected != x_actual or y_expected != y_actual:
        raise ValueError('expected origin (' + str(x_expected) + ', ' + str(y_expected) + '), but was (' + str(x_actual) + ', ' + str(y_actual) + ')')

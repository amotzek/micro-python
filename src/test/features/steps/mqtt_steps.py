from behave import *
import socket
import mqtt

@given('I open a socket to host "{host_name}" and port {port:d}')
def open_socket(context, host_name, port):
    address = (host_name, port)
    context.socket = socket.socket()
    context.socket.connect(address)
    context.socket_stream = context.socket.makefile(mode = 'rwb')

@when('I send a connect request with empty client id, user name and password')
def send_connect_request(context):
    client_id = ''
    user_name = ''
    password = ''
    request = mqtt.ConnectRequest(client_id, user_name, password)
    request.write_to(context.socket_stream)
    context.socket_stream.flush()

@when('I send a ping request')
def send_ping_request(context):
    request = mqtt.PingRequest()
    request.write_to(context.socket_stream)
    context.socket_stream.flush()

@when('I send a subscribe request with topic filter "{topic_filter}" and packet id {packet_id:n}')
def send_subscribe_request(context, topic_filter, packet_id):
    request = mqtt.SubscribeRequest(packet_id, topic_filter)
    request.write_to(context.socket_stream)
    context.socket_stream.flush()

@when('I send a publish request with topic "{topic}" and payload "{payload}"')
def send_publish_request(context, topic, payload):
    request = mqtt.PublishRequest(topic, payload)
    request.write_to(context.socket_stream)
    context.socket_stream.flush()

@when('I receive a response')
def receive_response(context):
    context.mqtt_response = mqtt.AbstractResponse.receive_from(context.socket_stream)

@then('the received response must have the return code "connection accepted"')
def connection_accepted(context):
    if not context.mqtt_response.connection_accepted():
        raise ValueError('cannot connect')

@then('the received response must be a ping response')
def ping_response(context):
    if type(context.mqtt_response) is not mqtt.PingResponse:
        raise ValueError('no ping response')

@then('the received response must have the return code "subscription accepted"')
def subscription_accepted(context):
    if not context.mqtt_response.subscription_accepted():
        raise ValueError('cannot subscribe ' + str(context.mqtt_response.return_code))

@then('the received response must have the packet id {packet_id:n}')
def has_packet_id(context, packet_id):
    if not context.mqtt_response.has_packet_id(packet_id):
        raise ValueError('wrong packet id ' + str(context.mqtt_response.packet_id))

@then('the received response must have the topic "{topic}"')
def has_topic(context, topic):
    if not context.mqtt_response.has_topic(topic):
        raise ValueError('unexpected topic ' + context.mqtt_response.topic)

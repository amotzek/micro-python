from behave import *

@given('I have no flags')
def init_flags(context):
    context.flags = {}

@given('I clear the flag "{name}"')
def clear_flag(context, name):
    context.flags[name] = False

@given('I set the flag "{name}"')
def set_flag(context, name):
    context.flags[name] = True

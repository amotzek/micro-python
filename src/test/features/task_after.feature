Feature: Execute tasks with delay

    Background:
        Given I have no tasks
          And I start the clock

    Scenario: Execute one task
         When I start the task "a" after 100 milliseconds
          And I execute all tasks
         Then I must have finished 1 tasks
          And I must have finished the task "a"
          And at least 100 milliseconds are elapsed

    Scenario: Execute four tasks in order
         When I start the task "a" after 90 milliseconds
          And I start the task "b" after 30 milliseconds
          And I start the task "c" after 120 milliseconds
          And I start the task "d" after 60 milliseconds
          And I execute all tasks
         Then I must have finished 4 tasks
          And I must have finished the task "b" at position 0
          And I must have finished the task "d" at position 1
          And I must have finished the task "a" at position 2
          And I must have finished the task "c" at position 3
          And at least 120 milliseconds are elapsed

Feature: Use arithmetic operators

  Background:
    Given I start with an empty page

  Scenario: Add two
    When I execute the logo commands "pendown; forward 5 + 5"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Subtract
    When I execute the logo commands "pendown; forward 15 - 5"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Multiply
    When I execute the logo commands "pendown; forward 2 * 5"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Divide
    When I execute the logo commands "pendown; forward 30 / 3"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Add three
    When I execute the logo commands "pendown; forward 3 + 3 + 4"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Multiply and add
    When I execute the logo commands "pendown; forward (3 * 3) + 1"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Add and multiply
    When I execute the logo commands "pendown; forward 1 + (3 * 3)"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Multiply parameter
    When I execute the logo commands "to doubleline :halfside [ forward 2 * :halfside ] pendown; doubleline 5"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Parameter multiply
    When I execute the logo commands "to doubleline :halfside [ forward :halfside * 2 ] pendown; doubleline 5"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

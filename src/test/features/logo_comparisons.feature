Feature: Use comparisons

  Background:
    Given I start with an empty page

  Scenario: Less
    When I execute the logo commands "pendown; if 1 < 2 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Not less
    When I execute the logo commands "pendown; if 2 < 1 [ forward 10 ]"
    Then the page must have 0 lines

  Scenario: Less or equal
    When I execute the logo commands "pendown; if 1 <= 2 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Equal or less
    When I execute the logo commands "pendown; if 2 <= 2 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Not less or equal
    When I execute the logo commands "pendown; if 2 <= 1 [ forward 10 ]"
    Then the page must have 0 lines

  Scenario: Equal
    When I execute the logo commands "pendown; if 1 = 1 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Not equal
    When I execute the logo commands "pendown; if 2 = 1 [ forward 10 ]"
    Then the page must have 0 lines

  Scenario: Not not equal
    When I execute the logo commands "pendown; if 2 != 1 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Greater or equal
    When I execute the logo commands "pendown; if 2 >= 1 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Equal or greater
    When I execute the logo commands "pendown; if 2 >= 2 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Not greater or equal
    When I execute the logo commands "pendown; if 1 >= 2 [ forward 10 ]"
    Then the page must have 0 lines

  Scenario: Greater
    When I execute the logo commands "pendown; if 2 > 1 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Not greater
    When I execute the logo commands "pendown; if 1 > 2 [ forward 10 ]"
    Then the page must have 0 lines

  Scenario: Less sum
    When I execute the logo commands "pendown; if 1 + 1 < 3 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Less sum sum
    When I execute the logo commands "pendown; if 1 + 1 < 2 + 1 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Not less sum sum
    When I execute the logo commands "pendown; if 2 + 2 < 2 + 1 [ forward 10 ]"
    Then the page must have 0 lines

  Scenario: Less sum product
    When I execute the logo commands "pendown; if 1 + 1 < 2 * 2 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Less sum product sum
    When I execute the logo commands "pendown; if 1 + 1 < (2 * 2) + 1 [ forward 10 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)

  Scenario: Not greater else
    When I execute the logo commands "pendown; ifelse 1 > 2 [ forward 10 ] [ forward 20 ]"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (20, 0)

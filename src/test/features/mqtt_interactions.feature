Feature: Interact with a MQTT broker

    Scenario: Subscribe and publish a message
        Given I open a socket to host "broker.mqttdashboard.com" and port 1883
         When I send a connect request with empty client id, user name and password
          And I receive a response
         Then the received response must have the return code "connection accepted"
         When I send a ping request
          And I receive a response
         Then the received response must be a ping response
         When I send a subscribe request with topic filter "lalala" and packet id 1
          And I receive a response
         Then the received response must have the return code "subscription accepted"
          And the received response must have the packet id 1
         When I send a publish request with topic "lalala" and payload "lololooO"
          And I receive a response
         Then the received response must have the topic "lalala"

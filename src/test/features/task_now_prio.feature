Feature: Execute priority tasks immediately

    Background:
        Given I have no tasks

    Scenario: Execute three tasks
         When I start the task "a" with priority 1 now
          And I start the task "b" with priority 3 now
          And I start the task "c" with priority 2 now
          And I execute at most one task
         Then I must have finished 1 tasks
          And I must have finished the task "b"

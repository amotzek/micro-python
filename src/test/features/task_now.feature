Feature: Execute tasks immediately

    Background:
        Given I have no tasks

    Scenario: Execute one task
         When I start the task "a" now
          And I execute all tasks
         Then I must have finished 1 tasks
          And I must have finished the task "a"

    Scenario: Execute three tasks
         When I start the task "a" now
          And I start the task "b" now
          And I start the task "c" now
          And I execute all tasks
         Then I must have finished 3 tasks
          And I must have finished the task "a"
          And I must have finished the task "b"
          And I must have finished the task "c"

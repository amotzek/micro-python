Feature: Draw lines

  Background:
    Given I start with an empty page

  Scenario: Draw a line
    When I execute the logo commands "pendown; forward 10"
    Then the page must have 1 lines
     And the page must have a line from (0, 0) to (10, 0)
     And the page must have a width of 10
     And the page must have an origin of (0, 0)

  Scenario: Draw a line again
    When I execute the logo commands "forward 10; pendown; forward 10"
    Then the page must have 1 lines
     And the page must have a line from (10, 0) to (20, 0)
     And the page must have a width of 10
     And the page must have an origin of (10, 0)

  Scenario: Draw two lines
    When I execute the logo commands "pendown; forward 10; right 90; forward 10"
    Then the page must have 2 lines
     And the page must have a line from (0, 0) to (10, 0)
     And the page must have a line from (10, 0) to (10, -10)
     And the page must have a width of 10
     And the page must have a height of 10
     And the page must have an origin of (0, -10)

  Scenario: Draw a square
    When I execute the logo commands "pendown; repeat 4 [ forward 10; right 90 ]"
    Then the page must have 4 lines
     And the page must have a line from (0, 0) to (10, 0)
     And the page must have a line from (10, 0) to (10, -10)
     And the page must have a line from (10, -10) to (0, -10)
     And the page must have a line from (0, -10) to (0, 0)
     And the page must have a width of 10
     And the page must have a height of 10
     And the page must have an origin of (0, -10)

# https://www.ietf.org/rfc/rfc4493.txt

from ucryptolib import aes as AES
from ucryptolib import MODE_ECB

def aes128_cmac(key, content, size):
    aes = AES(key, MODE_ECB)
    k1, k2 = generate_subkeys(aes)
    block_count = (size + 15) >> 4
    if block_count == 0:
        block_count = 1
        needs_padding = True
    else:
        needs_padding = (size & 15 > 0)
    x = bytearray(16)
    for index in range(1, block_count):
        y = xor(x, block(content, index))
        x = aes.encrypt(key, y)
    m_last = xor(padding(block(content, block_count)), k2) if needs_padding else xor(block(content, block_count), k1)
    y = xor(m_last, x)
    return aes.encrypt(key, y)

def generate_subkeys(aes):
    l = aes.encrypt(bytearray(16))
    if msb(l) == 0:
        k1 = shl(l)
    else:
        k1 = shl(l)
        k1[15] = k1[15] ^ 0x87
    if msb(k1) == 0:
        k2 = shl(k1)
    else:
        k2 = shl(k1)
        k2[15] = k2[15] ^ 0x87
    return (k1, k2)

def msb(arg):
    return 1 if arg[0] >= 128 else 0

def shl(arg):
    res = bytearray(16)
    carry = 0
    for index in range(15, -1, -1):
        value = (arg[index] << 1) | carry
        if value > 255:
            carry = 1
            res[index] = value & 255
        else:
            carry = 0
            res[index] = value
    return res

def xor(arg1, arg2):
    res = bytearray(16)
    for index in range(16):
        res[index] = arg1[index] ^ arg2[index]
    return res

def block(arg, index):
    low = (index - 1) << 4
    high = min(low + 16, len(arg))
    return arg[low:high]

def padding(arg):
    arg_len = len(arg)
    res = bytearray(16)
    res[0:arg_len] = arg
    res[arg_len] = 128
    return res

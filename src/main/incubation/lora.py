from rfc4493 import aes128_cmac

class AbstractRequest:

    def __init__(self, mtype, major):
        self.size = 1
        self.content = bytearray(128)
        self.content[0] = (mtype & 7) << 5 | (major & 3)
        for index in range(1, len(self.content)):
            self.content[index] = 0

    def _append_byte(self, value):
        self.content[self.size] = value
        self.size += 1

    def _append_short(self, value):
        self.content[self.size] = (value >> 8) & 255
        self.content[self.size + 1] = value & 255
        self.size += 2

    def _append_bytearray(self, value, length):
        self.content[self.size:self.size +  length] = value[0 : length]
        self.size += length

    def get_content(self):
        return self.content[:self.size]

class JoinRequest(AbstractRequest):

    def __init__(self, app_key, app_eui, dev_eui, dev_nonce):
        super().__init__(0, 0)  # Join Request, LoRaWAN R1
        self._append_bytearray(app_eui, 8)
        self._append_bytearray(dev_eui, 8)
        self._append_bytearray(dev_nonce, 2)
        self._append_bytearray(aes128_cmac(app_key, self.content, self.size), 4)

class AbstractResponse:

    def __init__(self):
        pass

    @staticmethod
    def parse_from(app_key, content):
        first_byte = content[0]
        mtype = (first_byte >> 5) & 7
        major = first_byte & 3
        if major != 0:
            raise ValueError('major version ' + str(major))
        if mtype == 1:
            return JoinAccept(app_key, content)
        if mtype == 3:
            return UnconfirmedDataNotification(content)
        if mtype == 5:
            return ConfirmedDataNotification(content)
        raise ValueError('message type ' + str(mtype))

class JoinAccept(AbstractResponse):

    def __init__(self, app_key, content):
        super().__init__()
        mic_expected = aes128_cmac(app_key, content, len(content) - 4)[0:4]
        mic_actual = content[-4:]
        if mic_expected != mic_actual:
            raise ValueError('mic expected ' + mic_expected.hex() + ' but was ' + mic_actual.hex())
        self.app_nonce = content[1:4]
        self.net_id = content[4:7]
        self.dev_addr = content[7:11]
        self.dl_settings = content[11]
        self.rx_delay = content[12]
        self.cf_list = content[13:-4] if len(content) > 17 else None

class UnconfirmedDataNotification(AbstractResponse):

    def __init__(self, content):
        super().__init__()

class ConfirmedDataNotification(AbstractResponse):

    def __init__(self, content):
        super().__init__()

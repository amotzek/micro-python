import utime
from micropython import const
from framebuf import FrameBuffer, GS4_HMSB
from machine import Pin

_LOCK = const(0xfd)
_DISP_OFF = const(0xae)
_START_LINE = const(0xa1)
_OFFSET = const(0xa2)
_REMAP = const(0xa0)
_MULTIPLEX_RATIO = const(0xa8)
_FN_SELECT_A = const(0xab)
_PHASE_LEN = const(0xb1)
_CLK_DIV = const(0xb3)
_PRECHARGE = const(0xbc)
_VCOM_DESEL = const(0xbe)
_SECOND_PRECHARGE = const(0xb6)
_FN_SELECT_B = const(0xd5)
_GRAYSCALE_LINEAR = const(0xb9)
_CONTRAST = const(0x81)
_NORM_MODE = const(0xa4)
_DISP_ON = const(0xaf)
_COL_ADDR = const(0x15)
_ROW_ADDR = const(0x75)

class SSD1327:

    def __init__(self, width, height, i2c, addr = 0x3c, rst = None):
        self.width = width
        self.height = height
        self.char_width = 8
        self.char_height = 8
        self.pixels = bytearray((width * height) >> 1)
        self.frame_buffer = FrameBuffer(self.pixels, width, height, GS4_HMSB)
        self.command = bytearray([0x80, 0x00])
        self.data = [b'\x40', self.pixels]
        self.i2c = i2c
        self.addr = addr
        self.rst = None if rst is None else Pin(rst, Pin.OUT)
        self._reset()
        self._setup()

    def on(self):
        self._write_command(_FN_SELECT_A)
        self._write_command(0x01)
        self._write_command(_DISP_ON)

    def off(self):
        self._write_command(_DISP_OFF)
        self._write_command(_FN_SELECT_A)
        self._write_command(0x00)

    def to_color(self, red, green, blue):
        return (red + green + blue) // 51

    def draw_string(self, x, y, s, color):
        self.frame_buffer.text(s, x, y, color)
        self._show()

    def draw_polyline(self, x, y, points, color):
        last_point = None
        for point in points:
            if last_point is not None:
                self.frame_buffer.line(x + last_point[0], y + last_point[1], x + point[0], y + point[1], color)
            last_point = point
        self._show()

    def scroll_up(self, height):
        self.frame_buffer.scroll(0, -height)
        self.frame_buffer.fill_rect(0, self.height - height, self.width, height, 0)
        self._show()

    def _reset(self):
        if self.rst is not None:
            self.rst.value(0)
            utime.sleep_ms(1)
            self.rst.value(1)
            utime.sleep_ms(50)

    def _setup(self):
        for command in (_LOCK, 0x12,  # unlock
                        _DISP_OFF,
                        _START_LINE, 0x00,
                        _OFFSET, 0x00,
                        _REMAP, 0x00,
                        _MULTIPLEX_RATIO, self.height - 1,
                        _FN_SELECT_A, 0x01,
                        _PHASE_LEN, 0x51,
                        _CLK_DIV, 0x01,
                        _PRECHARGE, 0x08,
                        _VCOM_DESEL, 0x07,
                        _SECOND_PRECHARGE, 0x01,
                        _FN_SELECT_B, 0x62,
                        _GRAYSCALE_LINEAR,
                        _CONTRAST, 0x7f,
                        _NORM_MODE):
            self._write_command(command)

    def _show(self):
        x0 = 0
        x1 = (self.width >> 1) - 1
        y0 = 0
        y1 = self.height - 1
        self._write_command(_COL_ADDR)
        self._write_command(x0)
        self._write_command(x1)
        self._write_command(_ROW_ADDR)
        self._write_command(y0)
        self._write_command(y1)
        self._write_data()

    def _write_command(self, command):
        self.command[1] = command
        self.i2c.writeto(self.addr, self.command)

    def _write_data(self):
        self.i2c.writevto(self.addr, self.data)

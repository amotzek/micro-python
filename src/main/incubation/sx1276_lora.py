import uos
from lora_states import NOT_JOINED, JOINING
from lora import JoinRequest

class SX1276LoRa:

    def __init__(self, sx1276, dev_eui, app_eui, app_key):
        self.sx1276 = sx1276
        self.dev_eui = dev_eui
        self.app_eui = app_eui
        self.app_key = app_key
        self.dev_nonce = None
        self.nwks_key = None
        self.apps_key = None
        self.state = NOT_JOINED
        self.sending = False
        self.receiving = False

    def send_join(self):
        self.sx1276.standby()
        self.receiving = False
        self.dev_nonce = uos.urandom(2)
        request = JoinRequest(self.app_key, self.app_eui, self.dev_eui, self.dev_nonce)
        self.sx1276.send_packet(request.get_content())
        self.sending = True
        self.state = JOINING

    def send_message(self, message):
        pass  # Todo

    def has_state_changed(self):
        return False  # Todo

    def get_state(self):
        return self.state

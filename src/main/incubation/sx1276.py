import utime
from micropython import const
from machine import Pin

_REG_FIFO = const(0x00)
_REG_OP_MODE = const(0x01)
_REG_FRF_MSB = const(0x06)
_REG_FRF_MID = const(0x07)
_REG_FRF_LSB = const(0x08)
_REG_PA_CONFIG = const(0x09)
_REG_LR_OCP = const(0x0b)
_REG_LNA = const(0x0c)
_REG_FIFO_ADDR_PTR = const(0x0d)
_REG_FIFO_TX_BASE_ADDR = const(0x0e)
_REG_FIFO_RX_BASE_ADDR = const(0x0f)
_REG_FIFO_RX_CURRENT_ADDR = const(0x10)
_REG_IRQ_FLAGS = const(0x12)
_REG_RX_NB_BYTES = const(0x13)
_REG_PKT_RSSI_VALUE = const(0x1a)
_REG_PKT_SNR_VALUE = const(0x1b)
_REG_FEI_MSB = const(0x1d)
_REG_FEI_LSB = const(0x1e)
_REG_PREAMBLE_DETECT = const(0x1f)
_REG_PREAMBLE_MSB = const(0x20)
_REG_PREAMBLE_LSB = const(0x21)
_REG_PAYLOAD_LENGTH = const(0x22)
_REG_MODEM_CONFIG = const(0x26)
_REG_RSSI_WIDEBAND = const(0x2c)
_REG_DETECTION_OPTIMIZE = const(0x31)
_REG_NODE_ADDR = const(0x33)
_REG_DETECTION_THRESHOLD = const(0x37)
_REG_SYNC_WORD = const(0x39)
_REG_IMAGE_CAL = const(0x3b)
_REG_DIO_MAPPING_1 = const(0x40)
_REG_VERSION = const(0x42)
_REG_PA_DAC = const(0x4d)

_MODE_SLEEP = const(0x00)
_MODE_LORA = const(0x80)
_MODE_STDBY = const(0x81)
_MODE_TX = const(0x83)
_MODE_RX_CONTINUOUS = const(0x85)
_MODE_RX_SINGLE = const(0x86)

_PA_BOOST = const(0x80)
_PA_RFO = const(0x70)

_IRQ_TX_DONE_MASK = const(0x08)
_IRQ_PAYLOAD_CRC_ERROR_MASK = const(0x20)
_IRQ_RX_DONE_MASK = const(0x40)

class SX1276:

    def __init__(self, spi, cs, rst, dio):
        self.spi = spi
        self.cs = Pin(cs, Pin.OUT)
        self.rst = Pin(rst, Pin.OUT)
        self.dio = Pin(dio, Pin.IN)
        self._reset()
        self._set_cs()
        #
        if self._get_version() != 0x12:
            raise NotImplementedError("wrong version")
        #
        self._setup()
        self._set_frequency()
        self._set_spreading_factor()
        self._set_bandwidth()
        self._set_auto_agc()

    def standby(self):
        self._write_register(_REG_OP_MODE, _MODE_STDBY)

    def send_packet(self, packet):
        packet_len = len(packet)
        if packet_len > 127:
            raise ValueError("packet too long")
        self._write_register(_REG_FIFO_ADDR_PTR, 0x80)
        self._write_register(_REG_PAYLOAD_LENGTH, packet_len)
        for i in range(packet_len):
            self._write_register(_REG_FIFO, packet[i])
        self._write_register(_REG_OP_MODE, _MODE_TX)

    def is_send_done(self):
        flags = self._read_register(_REG_IRQ_FLAGS)
        if flags & _IRQ_TX_DONE_MASK != 0:
            self._write_register(_REG_IRQ_FLAGS, _IRQ_TX_DONE_MASK)
            return True
        return False

    def listen(self):
        if self._read_register(_REG_OP_MODE) != _MODE_RX_SINGLE:
            self._write_register(_REG_FIFO_ADDR_PTR, 0x00)
            self._write_register(_REG_OP_MODE, _MODE_RX_SINGLE)

    def is_receive_done(self):
        flags = self._read_register(_REG_IRQ_FLAGS)
        if (flags & _IRQ_RX_DONE_MASK != 0) and (flags & _IRQ_PAYLOAD_CRC_ERROR_MASK == 0):
            self._write_register(_REG_IRQ_FLAGS, _IRQ_RX_DONE_MASK)
            return True
        return False

    def get_received_packet(self):
        packet_len = self._read_register(_REG_RX_NB_BYTES)
        if packet_len > 0:
            self._write_register(_REG_FIFO_ADDR_PTR, self._read_register(_REG_FIFO_RX_CURRENT_ADDR))
            packet = bytearray(packet_len)
            for i in range(packet_len):
                packet[i] = self._read_register(_REG_FIFO)
            return packet
        return None

    def _reset(self):
        self.rst.off()
        utime.sleep_ms(20)
        self.rst.on()
        utime.sleep_ms(50)
        self.rst.off()
        self.cs.on()

    def _set_cs(self):
        self.cs.on()

    def _get_version(self):
        return self._read_register(_REG_VERSION)

    def _setup(self):
        lna = self._read_register(_REG_LNA)
        for (register, value) in ((_REG_OP_MODE, _MODE_SLEEP),
                    (_REG_OP_MODE, _MODE_LORA), (_REG_PREAMBLE_DETECT, 0x25),
                    (_REG_PREAMBLE_MSB, 0x00), (_REG_PREAMBLE_LSB, 0x08),
                    (_REG_MODEM_CONFIG, 0x0c), (_REG_SYNC_WORD, 0x34),
                    (_REG_NODE_ADDR, 0x27), (_REG_IMAGE_CAL, 0x1d),
                    (_REG_FIFO_TX_BASE_ADDR, 0x80), (_REG_FIFO_RX_BASE_ADDR, 0x00),
                    (_REG_LNA, lna | 0x03), (_REG_PA_DAC, 0x87),
                    (_REG_PA_CONFIG, _PA_BOOST | 12)):
            self._write_register(register, value)

    def _set_frequency(self):
        self._write_register(_REG_FRF_MSB, 0xd9)  # 868.1 MHz
        self._write_register(_REG_FRF_MID, 0x06)
        self._write_register(_REG_FRF_LSB, 0x8b)

    def _set_spreading_factor(self):
        self._write_register(_REG_DETECTION_OPTIMIZE, 0xc3)
        self._write_register(_REG_DETECTION_THRESHOLD, 0x0a)
        self._write_register(_REG_FEI_LSB, 0x74)  # SF7

    def _set_bandwidth(self):
        self._write_register(_REG_FEI_MSB, 0x72)  # BW125

    def _set_auto_agc(self):
        self._write_register(_REG_MODEM_CONFIG, 0x04)

    def _read_register(self, register):
        return self._single_transfer(register & 0x7f, 0)

    def _write_register(self, register, value):
        return self._single_transfer(register | 0x80, value)

    def _single_transfer(self, register, value):
        read_buffer = bytearray(1)
        write_buffer = bytearray(2)
        write_buffer[0] = register
        write_buffer[1] = value
        self.cs.off()
        self.spi.write_readinto(write_buffer, read_buffer)
        self.cs.on()
        return read_buffer[0]

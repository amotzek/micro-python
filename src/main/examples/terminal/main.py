from cooperative_multitasking import Tasks
from network import WLAN, AP_IF
from machine import Pin, SPI
from ili9341 import ILI9341
from graphic_terminal import GraphicTerminal
from mqtt_client import MQTTClient

tasks = Tasks()
ap = WLAN(AP_IF)
ap.active(False)
ap = None
spi = SPI(sck = Pin(18), mosi = Pin(23), miso = Pin(19))
tft = ILI9341(spi)
tft.set_background(tft.to_color(0, 0, 0))
left_button = Pin(39, Pin.IN, Pin.PULL_UP)
terminal = GraphicTerminal(tasks, tft)
terminal.start()
visibility = 0

def is_hidden():
    return visibility == 0

def is_button_touched():
    return left_button.value() == 0

def is_button_released():
    return left_button.value() == 1

def on_button_touched():
    if is_hidden():
        show()
    tasks.when_for_then(is_button_released, 300, on_button_released)

def on_button_released():
    tasks.when_then(is_button_touched, on_button_touched)

def hide():
    global visibility
    visibility -= 1
    if visibility == 0:
        tft.off()

def show():
    global visibility
    visibility += 1
    if visibility == 1:
        tft.on()
    tasks.after(300000, hide)

def on_receive(topic_name, payload):
    try:
        terminal.print(payload[':message'])
        show()
        return
    except:
        pass
    try:
        terminal.execute_logo(payload[':logo'])
        show()
    except:
        pass

client = MQTTClient(tasks,
                    hostname = '...',  # MQTT broker
                    client_id = '...',
                    user_name = '...',
                    password = '...')
client.activate_wlan([('...', '...')])  # Name and password of WLAN
client.subscribe('...', on_receive)  # Topic
client.start()

terminal.execute_logo('pendown; repeat 4 [forward 8; right 90]')
on_button_touched()

while tasks.available():
    tasks.run()

from cooperative_multitasking import Tasks
from network import WLAN, AP_IF
from machine import Pin, SPI, I2C
from ili9341 import ILI9341
from lis3dh import LIS3DH
from graphic_terminal import GraphicTerminal
from math import atan2, sqrt, pi, degrees

tasks = Tasks()

ap = WLAN(AP_IF)
ap.active(False)
ap = None

spi = SPI(sck = Pin(18), mosi = Pin(23), miso = Pin(19))
tft = ILI9341(spi)
tft.set_background(tft.to_color(0, 0, 0))
tft.on()
terminal = GraphicTerminal(tasks, tft)
terminal.start()

i2c = I2C(scl = Pin(22), sda = Pin(21))
lis3dh = LIS3DH(i2c)

def tick():
    x, y, z = lis3dh.get_acceleration()
    phi = atan2(x, y)
    if phi < 0.0:
        phi += 2.0 * pi
    radius = sqrt(x * x + y * y)
    terminal.print(str(degrees(phi)))
    terminal.print(str(radius))
    tasks.after(30000, tick)

tasks.after(5000, tick)

while tasks.available():
    tasks.run()

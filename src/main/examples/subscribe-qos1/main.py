from network import WLAN, AP_IF
from machine import Pin
from cooperative_multitasking import Tasks
from mqtt_client import MQTTClient

ap = WLAN(AP_IF)
ap.active(False)
ap = None

led = Pin(0, Pin.OUT)

def led_on():
    led.value(0)

def led_off():
    led.value(1)

tasks = Tasks()

client = MQTTClient(tasks,
                    client_id = '...',
                    user_name = '...',
                    password = '...')
client.activate_wlan([('...', '...')])
client.start()

def is_connected():
    return client.is_connected()

def is_not_connected():
    return not client.is_connected()

def not_connected():
    led_off()
    tasks.when_then(is_connected, connected)

def connected():
    led_on()
    tasks.when_then(is_not_connected, not_connected)

def signal_message(topic, payload_object):
    led_off()
    tasks.after(10000, led_on)

client.subscribe('tuple/:ping', signal_message)

tasks.now(not_connected)

while tasks.available():
    tasks.run()

import utime
import machine

led = machine.Pin(2, machine.Pin.OUT)

while True:
    led.value(1)
    utime.sleep_ms(500)
    led.value(0)
    utime.sleep_ms(500)

from cooperative_multitasking import Tasks
from network import WLAN, AP_IF
from machine import Pin, I2C
from ssd1306 import SSD1306
from graphic_terminal import GraphicTerminal

tasks = Tasks()

ap = WLAN(AP_IF)
ap.active(False)
ap = None

i2c = I2C(scl=Pin(22), sda=Pin(21))
oled = SSD1306(128, 64, i2c)
oled.on()
terminal = GraphicTerminal(tasks, oled)
terminal.start()
terminal.execute_logo('pendown; repeat 4 [forward 8; right 90]')

def hihi():
    terminal.print('hihi')
    tasks.after(15000, hihi)

tasks.now(hihi)

while tasks.available():
    tasks.run()

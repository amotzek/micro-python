from network import WLAN, AP_IF
from machine import Pin, I2C
from pcf8591 import PCF8591_I2C
from cooperative_multitasking import Tasks
from mqtt_client import MQTTClient

ap = WLAN(AP_IF)
ap.active(False)
ap = None

tasks = Tasks()
client = MQTTClient(tasks,
                    client_id='...',
                    user_name='...',
                    password='...')
client.activate_wlan([('...', '...')])
client.start()

count = 0


def publish_count():
    client.publish('...', count)


led = Pin(0, Pin.OUT)  # D3
out1 = Pin(12, Pin.OUT)  # D6
out2 = Pin(13, Pin.OUT)  # D7
ena = Pin(14, Pin.OUT)  # D5
hall = Pin(15, Pin.IN)  # D8
i2c = I2C(scl=Pin(5), sda=Pin(4), freq=100000)  # D1/D2
adc = PCF8591_I2C(i2c)


def led_on():
    led.value(0)


def led_off():
    led.value(1)


def is_connected():
    return client.is_connected()


def is_not_connected():
    return not client.is_connected()


def not_connected():
    led_off()
    tasks.when_then(is_connected, connected)


def connected():
    led_on()
    tasks.when_then(is_not_connected, not_connected)


tasks.now(not_connected)


def increase_count():
    global count
    count += 1


def is_right():
    ain = adc.get_inputs()
    return ain[2] > ain[0] or hall.value() == 0


def is_left():
    ain = adc.get_inputs()
    return ain[1] > ain[0] and hall.value() == 1


def is_between():
    ain = adc.get_inputs()
    return ain[1] < ain[0] and ain[2] < ain[0] and hall.value() == 1


def is_dark():
    ain = adc.get_inputs()
    return ain[1] > ain[0] and ain[2] > ain[0]


def is_light():
    ain = adc.get_inputs()
    return ain[1] < ain[0] and ain[2] < ain[0]


def is_left_or_dark():
    return is_left() or is_dark()


def start_motor():
    ena.value(1)


def stop_motor():
    ena.value(0)


def set_direction_right():
    out1.value(1)
    out2.value(0)


def set_direction_left():
    out1.value(0)
    out2.value(1)


def drive_right():
    set_direction_right()
    tasks.now(start_motor, priority=1)
    tasks.only_one_of(tasks.when_then(is_right, wait_right),
                      tasks.after(15000, stop_motor))


def wait_right():
    stop_motor()
    increase_count()
    publish_count()
    tasks.only_one_of(tasks.after(300000, drive_left),
                      tasks.when_for_then(is_dark, 30000, park))


def drive_left():
    set_direction_left()
    tasks.now(start_motor, priority=1)
    tasks.only_one_of(tasks.when_then(is_left_or_dark, wait_left),
                      tasks.after(15000, stop_motor))


def wait_left():
    stop_motor()
    increase_count()
    publish_count()
    tasks.after(300000, drive_right)


def park():
    set_direction_left()
    tasks.now(start_motor)
    tasks.after(2000, stop_middle)


def stop_middle():
    stop_motor()
    tasks.when_for_then(is_light, 30000, drive_right)


def init():
    if is_dark():
        stop_middle()
    elif is_left() or is_between():
        drive_right()
    else:
        drive_left()


tasks.now(init)

while tasks.available():
    tasks.run()

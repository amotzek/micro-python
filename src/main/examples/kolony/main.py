from machine import Pin, I2C
from ssd1306 import SSD1306_I2C
import uos
import network
import cooperative_multitasking
import kolony

user_name = 'espresso898'

i2c = I2C(scl = Pin(5), sda = Pin(4), freq = 300000)
display = SSD1306_I2C(128, 32, i2c)
kolony_x = 0
kolony_y = 0
twinker_count = 0

def kolony_awake():
    global twinker_count
    display.fill(0)
    kolony.eyes_open(display, kolony_x, kolony_y)
    kolony.mouth_flat(display, kolony_x, kolony_y)
    display.show()
    if twinker_count > 5:
        twinker_count = 0
        tasks.after(5000, kolony_away)
    else:
        twinker_count += 1
        tasks.after(500 + randint(0, 9500), kolony_twinker)

def kolony_twinker():
    display.fill(0)
    kolony.eyes_closed(display, kolony_x, kolony_y)
    kolony.mouth_flat(display, kolony_x, kolony_y)
    display.show()
    tasks.after(150, kolony_awake)

def kolony_away():
    global kolony_x, kolony_y
    kolony_x = randint(-7, 101)
    kolony_y = randint(-6, 4)
    display.fill(0)
    display.show()
    tasks.after(10000, kolony_awake)

def randint(low, high):
    s = 0
    bs = uos.urandom(4)
    for b in bs:
        s = (s << 8) | b
    return low + (s % (high - low + 1))

ap = network.WLAN(network.AP_IF)
ap.active(False)
ap = None

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.config(dhcp_hostname = user_name)

tasks = cooperative_multitasking.Tasks()
tasks.now(kolony_awake)

while tasks.available():
    tasks.run()

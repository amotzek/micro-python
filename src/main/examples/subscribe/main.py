import machine
import network
import usocket
import uselect
import mqtt
import cooperative_multitasking

led = machine.Pin(2, machine.Pin.OUT)
ssid_passwords = [('...', '...'), ('...', '...')]
hostname = 'simplysomethings.de'
client_id = '...'
user_name = '...'
password = '...'
topic_filter = 'tuple/:ping'
broker = None
stream = None

def connect_wlan():
    try:
        access_points = wlan.scan()
        for ssid_password in ssid_passwords:
            ssid = ssid_password[0]
            for access_point in access_points:
                if access_point[0] == bytes(ssid, 'ascii'):
                    wlan.connect(ssid, ssid_password[1])
                    tasks.after(3000, create_socket)
                    return
    except Exception:
        pass
    tasks.after(30000, connect_wlan)

def create_socket():
    global broker, stream
    try:
        broker = usocket.socket()
        address = usocket.getaddrinfo(hostname, 1883)[0][-1]
        broker.connect(address)
        stream = broker.makefile('rwb')
        tasks.now(send_connect)
    except Exception:
        tasks.after(30000, connect_wlan)

def send_connect():
    try:
        request = mqtt.ConnectRequest(client_id, user_name, password)
        request.write_to(stream)
        tasks.only_one_of(tasks.when_then(can_read_socket, acknowledge_connect),
                          tasks.after(3000, close_socket))
    except Exception:
        tasks.now(close_socket)

def can_read_socket():
    poll = uselect.poll()
    poll.register(broker, uselect.POLLIN)
    return len(poll.poll(0)) > 0

def acknowledge_connect():
    try:
        response = mqtt.AbstractResponse.receive_from(stream)
        if response.connection_accepted():
            tasks.now(subscribe)
        else:
            tasks.now(close_socket)
    except Exception:
        tasks.now(close_socket)

def subscribe():
    try:
        request = mqtt.SubscribeRequest(1, topic_filter)
        request.write_to(stream)
        tasks.only_one_of(tasks.when_then(can_read_socket, acknowledge_subscribe),
                          tasks.after(3000, close_socket))
    except Exception:
        tasks.now(close_socket)

def acknowledge_subscribe():
    try:
        response = mqtt.AbstractResponse.receive_from(stream)
        if response.subscription_accepted() and response.has_packet_id(1):
            tasks.now(led_on)
            tasks.only_one_of(tasks.when_then(can_read_socket, receive_publish),
                              tasks.after(3600000, close_socket))
        else:
            tasks.now(close_socket)
    except Exception:
        tasks.now(close_socket)

def receive_publish():
    try:
        response = mqtt.AbstractResponse.receive_from(stream)
        tasks.now(led_off)
        tasks.after(10000, led_on)
        tasks.only_one_of(tasks.when_then(can_read_socket, receive_publish),
                          tasks.after(3600000, close_socket))
    except Exception:
        tasks.now(close_socket)

def close_socket():
    global broker, stream
    broker.close()
    broker = None
    stream = None
    tasks.now(led_off)
    tasks.after(30000, create_socket)

def led_on():
    led.value(0)

def led_off():
    led.value(1)

ap = network.WLAN(network.AP_IF)
ap.active(False)
ap = None

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.config(dhcp_hostname = user_name)

tasks = cooperative_multitasking.Tasks()
tasks.now(led_off)
tasks.now(connect_wlan)

while tasks.available():
    tasks.run()

from cooperative_multitasking import Tasks
from machine import Pin, SPI, UART
from rhf76052 import RHF76052
from lora_states import JOINED, SENDING, SENT
from lora_client import LoRaClient
from network import WLAN, AP_IF
from ili9341 import ILI9341
from graphic_terminal import GraphicTerminal

count = 0
tasks = Tasks()
ap = WLAN(AP_IF)
ap.active(False)
ap = None
spi = SPI(sck = Pin(18), mosi = Pin(23), miso = Pin(19))
tft = ILI9341(spi)
tft.set_background(tft.to_color(0, 0, 0))
tft.on()
terminal = GraphicTerminal(tasks, tft)
terminal.start()
uart = UART(2, 9600, bits = 8, parity = None, stop = 1)
modem = RHF76052(uart,
                 b'\x00\x00\x00\x00\x00\x00\x00\x00',  # deveui
                 b'\x00\x00\x00\x00\x00\x00\x00\x00',  # appeui
                 b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')  # appkey
client = LoRaClient(tasks, modem)

def on_state_change(next_state):
    terminal.print(str(next_state))

def is_connected():
    return client.get_state() in (JOINED, SENDING, SENT)

def send_count():
    global count
    count += 1
    client.send_text_message('{"activity-count": ' + str(count) + '}', confirmed = (count % 29 == 1))  # Fair use: only 10 confirmed messages a day
    tasks.when_for_then(is_connected, 300000, send_count)

client.set_state_change_listener(on_state_change)
tasks.after(60000, send_count)

while tasks.available():
    tasks.run()

import uos
from cooperative_multitasking import Tasks
from network import WLAN, AP_IF
from mqtt_client import MQTTClient
from machine import Pin, I2C
from ssd1306 import SSD1306_I2C
from scroller import Scroller
import kolony

tasks = Tasks()

ap = WLAN(AP_IF)
ap.active(False)
ap = None

client = MQTTClient(tasks,
                    hostname = '...',
                    client_id = '...',
                    user_name = '...',
                    password = '...')
client.activate_wlan([('...', '...')])
client.start()

i2c = I2C(scl = Pin(5), sda = Pin(4), freq = 300000)
display = SSD1306_I2C(128, 32, i2c)
scroller = None
kolony_x = 0
kolony_y = 0
twinker_count = 0

def receive_message(topic, payload_object):
    global scroller
    scroller = Scroller(display, payload_object[':message'])

client.subscribe('...', receive_message)

def is_connected():
    return client.is_connected()

def is_not_connected():
    return not client.is_connected()

def has_message():
    return scroller is not None

def kolony_sleep():
    kolony_move()
    display.fill(0)
    kolony.eyes_closed(display, kolony_x, kolony_y)
    kolony.mouth_flat(display, kolony_x, kolony_y)
    display.show()
    tasks.only_one_of(tasks.when_then(is_connected, kolony_awake),
                      tasks.after(60000, kolony_sleep))

def kolony_awake():
    display.fill(0)
    kolony.eyes_open(display, kolony_x, kolony_y)
    kolony.mouth_flat(display, kolony_x, kolony_y)
    display.show()
    tasks.only_one_of(tasks.when_then(is_not_connected, kolony_sleep),
                      tasks.when_then(has_message, scroll_message),
                      tasks.after(500 + randint(0, 9500), kolony_twinker))

def kolony_twinker():
    global twinker_count
    twinker_count += 1
    if twinker_count <= 5:
        display.fill(0)
        kolony.eyes_closed(display, kolony_x, kolony_y)
        kolony.mouth_flat(display, kolony_x, kolony_y)
        display.show()
    else:
        twinker_count = 0
        kolony_move()
    tasks.after(150, kolony_awake)

def kolony_move():
    global kolony_x, kolony_y
    kolony_x = randint(-7, 101)
    kolony_y = randint(-6, 4)

def scroll_message():
    global scroller
    scroller.scroll()
    display.show()
    if is_connected():
        tasks.after(100, scroll_message)
    else:
        scroller = None
        tasks.now(kolony_sleep)

def randint(low, high):
    s = 0
    bs = uos.urandom(4)
    for b in bs:
        s = (s << 8) | b
    return low + (s % (high - low + 1))

tasks.now(kolony_sleep)

while tasks.available():
    tasks.run()

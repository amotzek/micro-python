from micropython import const
import utime
import ustruct

_CTRL_REG5 = const(0x24)
_CTRL_REG1 = const(0x20)
_CTRL_REG4 = const(0x22)
_OUT_X_L = const(0x28)

_RESET = const(0x80)
_ODR_25_HZ = const(0x30)
_XYZ_EN = const(0x07)
_UPDATE_WHEN_READ = const(0x80)
_HIRES = const(0x08)

_DIVIDER_2_G = const(16380)

class LIS3DH:

    def __init__(self, i2c, addr = 0x18):
        self.i2c = i2c
        self.addr = addr
        self._reset()
        self._setup()

    def _reset(self):
        self._write_register(_CTRL_REG5, _RESET)
        utime.sleep_ms(10)

    def _setup(self):
        self._write_register(_CTRL_REG1, _ODR_25_HZ | _XYZ_EN)
        self._write_register(_CTRL_REG4, _UPDATE_WHEN_READ | _HIRES)

    def get_acceleration(self):
        x, y, z = ustruct.unpack('<hhh', self._read_register(_OUT_X_L, 6))
        return (x / _DIVIDER_2_G, y / _DIVIDER_2_G, z / _DIVIDER_2_G)

    def _write_register(self, register, value):
        writebuffer = bytearray(2)
        writebuffer[0] = register
        writebuffer[1] = value
        self.i2c.writeto(self.addr, writebuffer)

    def _read_register(self, register, size):
        writebuffer = bytearray(1)
        writebuffer[0] = register | 0x80
        self.i2c.writeto(self.addr, writebuffer)
        readbuffer = self.i2c.readfrom(self.addr, size)
        return readbuffer

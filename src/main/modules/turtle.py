from math import sin, cos, radians, floor

class Turtle:

    def __init__(self):
        self.x_src = 0
        self.y_src = 0
        self.heading = 0
        self.color = (255, 255, 255)
        self.line = None
        self.colored_lines = []
        self.x_min = None
        self.y_min = None
        self.x_max = None
        self.y_max = None

    def is_pen_up(self):
        return self.line is None

    def is_pen_down(self):
        return self.line is not None

    def pen_down(self):
        if self.is_pen_up():
            self.line = [(self.x_src, self.y_src)]
            if self.x_min is None:
                self.x_min = self.x_src
                self.y_min = self.y_src
                self.x_max = self.x_src
                self.y_max = self.y_src

    def pen_up(self):
        if self.is_pen_down():
            if len(self.line) > 1:
                self.colored_lines.append((self.line, self.color))
            self.line = None

    def set_pen_color(self, red, green, blue):
        color_dest = (red, green, blue)
        if self.color != color_dest:
            if self.is_pen_down():
                self.pen_up()
                self.pen_down()
            self.color = color_dest

    def set_x_y(self, x_dest, y_dest):
        if self.is_pen_down():
            self.line.append((x_dest, y_dest))
            if x_dest < self.x_min:
                self.x_min = x_dest
            if x_dest > self.x_max:
                self.x_max = x_dest
            if y_dest < self.y_min:
                self.y_min = y_dest
            if y_dest > self.y_max:
                self.y_max = y_dest
        self.x_src = x_dest
        self.y_src = y_dest

    def get_colored_lines(self):
        self.pen_up()
        return self.colored_lines

    def get_bounds(self):
        return (0, 0, 0, 0) if self.x_min is None else (self.x_min, self.y_min, self.x_max - self.x_min, self.y_max - self.y_min)

    def forward(self, distance):
        heading_rad = radians(self.heading)
        x_dest = Turtle._round(self.x_src + distance * cos(heading_rad))
        y_dest = Turtle._round(self.y_src + distance * sin(heading_rad))
        self.set_x_y(x_dest, y_dest)

    def back(self, distance):
        self.forward(- distance)

    def set_heading(self, heading_dest):
        while heading_dest >= 360:
            heading_dest -= 360
        while heading_dest < 0:
            heading_dest += 360
        self.heading = heading_dest

    def left(self, angle):
        self.set_heading(self.heading + angle)

    def right(self, angle):
        self.left(- angle)

    @staticmethod
    def _round(value):
        return int(floor(value + 0.5))

# For MakerHawk M5Stack Lora Modul use
# uart = UART(2, 9600, bits = 8, parity = None, stop = 1)
#
# Attention: This module is not compatible with M5Stack PSRAM 2.0 Fire,
# because that one uses GPIO 16 and 17 for PSRAM

from lora_states import NOT_JOINED, JOINING, JOINED, CANNOT_JOIN, SENDING, SENT, RETRY, NOT_SENT
from utime import sleep_ms

class RHF76052:

    def __init__(self, uart, dev_eui, app_eui, app_key, region = 'EU868', clazz = 'A'):
        self.uart = uart
        self.dev_eui = dev_eui
        self.app_eui = app_eui
        self.app_key = app_key
        self.region = region
        self.clazz = clazz
        self.line = None
        self.join_failed = False
        self.confirmed = False
        self.state = None
        self._reset()
        self._write_otaa_mode()
        self._write_dev_eui()
        self._write_app_eui()
        self._write_app_key()
        self._write_region()
        self._write_power()
        self._write_adaptive_data_rate()
        self._write_class()

    def send_join(self):
        self._write_join()

    def send_message(self, message, confirmed):
        self._write_message(message, confirmed)

    def has_state_changed(self):
        return self._available()

    def get_state(self):
        if self.state == JOINING:
            self._read_join()
        elif self.state == SENDING:
            self._read_message()
        return self.state

    def _reset(self):
        self._write_line('AT')
        self._read_expected_line('+AT: OK')

    def _write_otaa_mode(self):
        self._write_read_command('MODE', 'LWOTAA')

    def _write_dev_eui(self):
        self._write_read_command('ID', RHF76052._to_hex_string('DevEui, ', self.dev_eui))

    def _write_app_eui(self):
        self._write_read_command('ID', RHF76052._to_hex_string('AppEui, ', self.app_eui))

    def _write_app_key(self):
        self._write_read_command('KEY', RHF76052._to_hex_string('AppKey, ', self.app_key))

    def _write_region(self):
        self._write_read_command('DR', self.region)

    def _write_power(self):
        self._write_read_command('POWER', '10')

    def _write_adaptive_data_rate(self):
        self._write_read_command('ADR', 'ON')

    def _write_class(self):
        self._write_read_command('CLASS', self.clazz)

    def _write_join(self):
        self._write_line('AT+JOIN=FORCE')
        self.state = JOINING

    def _write_message(self, message, confirmed):
        self._write_command('CMSGHEX' if confirmed else 'MSGHEX', RHF76052._to_hex_string('', message))
        self.confirmed = confirmed
        self.state = SENDING

    def _available(self):
        return self.uart.any() > 1

    def _read_join(self):
        self._read_line()
        if self._line_starts_with('+JOIN'):
            if self._line_contains('No band'):
                self.state = CANNOT_JOIN
            elif self._line_ends_with('Join failed'):
                self.join_failed = True
            elif self._line_ends_with('Done'):
                if self.join_failed:
                    self.join_failed = False
                    self.state = CANNOT_JOIN
                else:
                    self.state = JOINED
            self._clear_line()
        else:
            self.state = NOT_JOINED

    def _read_message(self):
        self._read_line()
        if self._line_starts_with('+CMSGHEX' if self.confirmed else '+MSGHEX'):
            if self._line_ends_with('Done'):
                self.state = SENT
            elif self._line_ends_with('LoRaWAN modem is busy') \
              or self._line_contains('No free channel') \
              or self._line_contains('No band') \
              or self._line_ends_with('DR error') \
              or self._line_contains('Length error'):
                self.state = RETRY
            self._clear_line()
        else:
            self.state = NOT_SENT

    def _write_read_command(self, command, argument = None):
        self._write_command(command, argument = argument)
        self._read_expected_line('+' + command + ':')

    def _write_command(self, command, argument):
        line = 'AT+'
        line += command
        if argument is not None:
            line += '='
            line += argument
        self._write_line(line)

    def _write_line(self, line):
        self.uart.write(line)
        self.uart.write('\r\n')

    def _read_line(self):
        if self.uart.any() == 0:
            sleep_ms(300)
        self.line = str(self.uart.readline(), 'ASCII')[:-2]

    def _read_expected_line(self, expected_line):
        self._read_line()
        if not self._line_starts_with(expected_line) or self._line_contains('ERROR'):
            raise ValueError('expected ' + expected_line + ', but got ' + self.line)
        self._clear_line()

    def _line_starts_with(self, part):
        return self.line.startswith(part)

    def _line_contains(self, part):
        return self.line.find(part) >= 0

    def _line_ends_with(self, part):
        return self.line.endswith(part)

    def _clear_line(self):
        self.line = None

    @staticmethod
    def _to_hex_string(prefix, values):
        string = prefix + '"'
        for value in values:
            string += ('%02x' % value)
        string += '"'
        return string

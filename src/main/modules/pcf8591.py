from micropython import const

DA_ACTIVE = const(0x40)
AUTO_INC = const(0x04)

class PCF8591_I2C:

    def __init__(self, i2c, addr = 0x48):
        self.i2c = i2c
        self.addr = addr
        self.read_buffer = None
        self.write_buffer = bytearray(2)
        self.write_buffer[0] = DA_ACTIVE | AUTO_INC
        self.write_buffer[1] = 0

    def get_inputs(self):
        self._sync()
        return self.read_buffer[1:]

    def set_output(self, value):
        self.write_buffer[1] = value
        self._sync()

    def _sync(self):
        self.i2c.writeto(self.addr, self.write_buffer)
        self.read_buffer = self.i2c.readfrom(self.addr, 5)
from framebuf import FrameBuffer, MONO_HLSB

class Scroller:

    def __init__(self, display, message):
        self.display = display
        self.message = message
        self.message_length = len(message)
        self.offset = 0

    def scroll(self, delta = 1):
        self.display.scroll(-delta, 0)
        index = self.offset >> 5
        if index < self.message_length:
            self._magnify_letter(self.display.width - self.offset % 32, 0, index)
        if index > self.message_length:
            self.offset = 0
        else:
            self.offset += delta

    def at_start(self):
        return self.offset == 0

    def _magnify_letter(self, x_origin, y_origin, index):
        letter = FrameBuffer(bytearray(8), 8, 8, MONO_HLSB)
        letter.text(self.message[index], 0, 0)
        for x in range(0, 8):
            for y in range(0, 8):
                self.display.fill_rect(x_origin + (x << 2), y_origin + (y << 2), 4, 4, letter.pixel(x, y))

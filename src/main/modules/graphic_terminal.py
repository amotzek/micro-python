from turtle import Turtle
from logo import Logo

def _create_print_char_task(terminal, char, color):
    return lambda : terminal._print_char(char, color)

def _create_free_height_task(terminal, height):
    return lambda : terminal._free_height(height)

def _create_draw_line_task(terminal, x_offset, y_offset, points, color):
    return lambda : terminal._draw_line(x_offset, y_offset, points, color)

def _create_add_size_task(terminal, width, height):
    return lambda : terminal._add_size(width, height)

def _create_filled_queue_test(terminal):
    return lambda : terminal._filled_queue()

def _create_dequeue_task(terminal):
    return lambda : terminal._dequeue()

class GraphicTerminal:

    def __init__(self, tasks, display):
        self.tasks = tasks
        self.display = display
        self.queue = []
        self.x = 0
        self.y = 0

    def print(self, message, color_red = 255, color_green = 255, color_blue = 255):
        if len(message) > 0:
            color = self.display.to_color(color_red, color_green, color_blue)
            for char in message:
                self.queue.append(_create_print_char_task(self, char, color))
            message = None
            self.queue.append(_create_print_char_task(self, ' ', color))

    def execute_logo(self, text):
        try:
            turtle = Turtle()
            logo = Logo(turtle)
            logo.execute(text)
            bounds = turtle.get_bounds()
            colored_lines = turtle.get_colored_lines()
            x_offset = 1 - bounds[0]
            y_offset = 1 - bounds[1]
            width = bounds[2] + 2
            height = bounds[3] + 2
            bounds = None
            logo = None
            turtle = None
            if width > self.display.width or height > self.display.height:
                raise ValueError('image size ' + str(width) + 'x' + str(height) + ' exceeds display size')
            self.queue.append(_create_free_height_task(self, height))
            for colored_line in colored_lines:
                points = colored_line[0]
                color_red, color_green, color_blue = colored_line[1]
                color = self.display.to_color(color_red, color_green, color_blue)
                self.queue.append(_create_draw_line_task(self, x_offset, y_offset, points, color))
            self.queue.append(_create_add_size_task(self, width, height))
        except Exception as e:
            self.print(type(e).__name__, color_green = 0, color_blue = 0)
            self.print(str(e), color_green = 0, color_blue = 0)

    def start(self):
        self.tasks.when_for_then(_create_filled_queue_test(self), 100, _create_dequeue_task(self))

    def _filled_queue(self):
        return len(self.queue) > 0

    def _dequeue(self):
        task = self.queue.pop(0)
        task()
        self.start()

    def _print_char(self, char, color):
        if self.x + self.display.char_width >= self.display.width:
            self.x = 0
            self.y += self.display.char_height
        if self.y + self.display.char_height >= self.display.height:
            self.display.scroll_up(self.display.char_height)
            self.y -= self.display.char_height
        self.display.draw_string(self.x, self.y, char, color = color)
        self.x += self.display.char_width

    def _free_height(self, height):
        if self.x > 0:
            self.y += self.display.char_height
            self.x = 0
        if self.y + height >= self.display.height:
            self.display.scroll_up(self.y + height - self.display.height)
            self.y = self.display.height - height

    def _draw_line(self, x, y, points, color):
        self.display.draw_polyline(x + self.x, y + self.y, points, color)

    def _add_size(self, width, height):
        self.x += width
        self.y += max(height - self.display.char_height, 0)

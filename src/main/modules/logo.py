import ure

class Tokenizer:

    def __init__(self, text):
        self.text = text
        self.index = 0
        self.tokens = []
        self.regexp = ure.compile('[ \t\r\n]*(:?[a-z][a-z0-9_]*|-?[0-9]+|<=?|>=?|=|\\!=|\\+|-|\\*|/|\\[|\\]|\\(|\\)|;)')

    def next_token(self):
        if len(self.tokens) > 0:
            return self.tokens.pop()
        if self.index >= len(self.text):
            return None
        match = self.regexp.match(self.text[self.index:])
        if match is None:
            return None
        self.index += len(match.group(0))  # match.end(1)
        return match.group(1)

    def push_token(self, token):
        if token is not None:
            self.tokens.append(token)

    @staticmethod
    def is_name(token):
        if token is None:
            return False
        first = token[0]
        return first >= 'a' and first <= 'z'

    @staticmethod
    def is_parameter(token):
        if token is None:
            return False
        return token[0] == ':'

    @staticmethod
    def is_number(token):
        if token is None:
            return False
        if token == '0':
            return True
        first = token[0]
        return first == '-' or (first >= '1' and first <= '9')

    @staticmethod
    def is_opening_square_bracket(token):
        if token is None:
            return False
        return token == '['

    @staticmethod
    def is_closing_square_bracket(token):
        if token is None:
            return False
        return token == ']'

    @staticmethod
    def is_opening_round_bracket(token):
        if token is None:
            return False
        return token == '('

    @staticmethod
    def is_closing_round_bracket(token):
        if token is None:
            return False
        return token == ')'

    @staticmethod
    def is_semicolon(token):
        if token is None:
            return False
        return token == ';'

    @staticmethod
    def is_operator(token):
        if token is None:
            return False
        return token == '+' or token == '-' or token == '*' or token == '/'

    @staticmethod
    def is_comparison(token):
        if token is None:
            return False
        return token == '<' or token == '<=' or token == '=' or token == '!=' or token == '>' or token == '>='

class Parser:

    def __init__(self, tokenizer):
        self.tokenizer = tokenizer

    def parse_top_level_statement(self):
        token = self.tokenizer.next_token()
        if token == 'to':
            return self.parse_procedure()
        self.tokenizer.push_token(token)
        return self.parse_statement()

    def parse_statement(self):
        token = self.tokenizer.next_token()
        if token is None:
            return None
        if token == 'repeat':
            return self.parse_repeat()
        if token == 'if':
            return self.parse_if()
        if token == 'ifelse':
            return self.parse_if_else()
        self.tokenizer.push_token(token)
        return self.parse_call()

    def parse_procedure(self):
        name = self.parse_name()
        parameters = self.parse_parameters()
        statements = self.parse_statements()
        return ProcedureDefinition(name, parameters, statements)

    def parse_repeat(self):
        count = self.parse_arithmetic_expression()
        statements = self.parse_statements()
        return Repeat(count, statements)

    def parse_if(self):
        condition = self.parse_boolean_expression()
        then_statements = self.parse_statements()
        return IfElse(condition, then_statements)

    def parse_if_else(self):
        condition = self.parse_boolean_expression()
        then_statements = self.parse_statements()
        else_statements = self.parse_statements()
        return IfElse(condition, then_statements, else_statements)

    def parse_call(self):
        name = self.parse_name()
        arguments = self.parse_arguments()
        return Call(name, arguments)

    def parse_name(self):
        token = self.tokenizer.next_token()
        if not Tokenizer.is_name(token):
            raise ValueError('expected name, but found ' + token)
        return token

    def parse_arguments(self):
        arguments = []
        token = self.tokenizer.next_token()
        while not (token is None or Tokenizer.is_semicolon(token) or Tokenizer.is_closing_square_bracket(token)):
            self.tokenizer.push_token(token)
            arguments.append(self.parse_arithmetic_expression())
            token = self.tokenizer.next_token()
        if Tokenizer.is_closing_square_bracket(token):
            self.tokenizer.push_token(token)
        return arguments

    def parse_parameters(self):
        parameters = []
        token = self.tokenizer.next_token()
        while Tokenizer.is_parameter(token):
            parameters.append(token)
            token = self.tokenizer.next_token()
        self.tokenizer.push_token(token)
        return parameters

    def parse_statements(self):
        token = self.tokenizer.next_token()
        if not Tokenizer.is_opening_square_bracket(token):
            raise ValueError('expected "[", but found ' + token)
        statements = []
        token = self.tokenizer.next_token()
        while not Tokenizer.is_closing_square_bracket(token):
            self.tokenizer.push_token(token)
            statements.append(self.parse_statement())
            token = self.tokenizer.next_token()
        return statements

    def parse_arithmetic_expression(self):
        token = self.tokenizer.next_token()
        if Tokenizer.is_number(token):
            left = Constant(int(token))
        elif Tokenizer.is_parameter(token):
            left = Parameter(token)
        elif Tokenizer.is_opening_round_bracket(token):
            left = self.parse_bracketed_arithmetic_expression()
        else:
            raise ValueError('expected arithmetic expression, but found ' + token)
        token = self.tokenizer.next_token()
        if Tokenizer.is_operator(token):
            right = self.parse_arithmetic_expression()
            return ArithmeticExpression(left, token, right)
        else:
            self.tokenizer.push_token(token)
            return left

    def parse_boolean_expression(self):
        left = self.parse_comparison()
        token = self.tokenizer.next_token()
        while token == 'or':
            right = self.parse_comparison()
            left = DisjunctionExpression(left, right)
            token = self.tokenizer.next_token()
        self.tokenizer.push_token(token)
        return left

    def parse_comparison(self):
        left = self.parse_arithmetic_expression()
        token = self.tokenizer.next_token()
        if Tokenizer.is_comparison(token):
            right = self.parse_arithmetic_expression()
            return ComparisonExpression(left, token, right)
        else:
            raise ValueError('expected comparison, but found ' + token)

    def parse_bracketed_arithmetic_expression(self):
        expression = self.parse_arithmetic_expression()
        token = self.tokenizer.next_token()
        if Tokenizer.is_closing_round_bracket(token):
            return expression
        else:
            raise ValueError('expected ")", but found ' + token)

class ProcedureDefinition:

    def __init__(self, name, parameters, statements):
        self.name = name
        self.parameters = parameters
        self.statements = statements

    def execute(self, procedures, environment):
        procedures[self.name] = Procedure(self.parameters, self.statements)

class Repeat:

    def __init__(self, count, statements):
        self.count = count
        self.statements = statements

    def execute(self, procedures, environment):
        count_value = self.count.evaluate(environment)
        for index in range(count_value):
            for statement in self.statements:
                statement.execute(procedures, environment)

class IfElse:

    def __init__(self, condition, then_statements, else_statements = None):
        self.condition = condition
        self.then_statements = then_statements
        self.else_statements = else_statements

    def execute(self, procedures, environment):
        condition_value = self.condition.evaluate(environment)
        if condition_value:
            for statement in self.then_statements:
                statement.execute(procedures, environment)
        elif self.else_statements is not None:
            for statement in self.else_statements:
                statement.execute(procedures, environment)

class Call:

    def __init__(self, name, arguments):
        self.name = name
        self.arguments = arguments

    def execute(self, procedures, environment):
        procedure = procedures[self.name]
        argument_values = []
        for argument in self.arguments:
            argument_values.append(argument.evaluate(environment))
        procedure.invoke(procedures, argument_values)

class Procedure:

    def __init__(self, parameters, statements):
        self.parameters = parameters
        self.statements = statements

    def invoke(self, procedures, argument_values):
        environment = {}
        for parameter in self.parameters:
            environment[parameter] = argument_values.pop(0)
        for statement in self.statements:
            statement.execute(procedures, environment)

class Forward:

    def __init__(self, turtle):
        self.turtle = turtle

    def invoke(self, procedures, argument_values):
        self.turtle.forward(argument_values[0])

class Back:

    def __init__(self, turtle):
        self.turtle = turtle

    def invoke(self, procedures, argument_values):
        self.turtle.back(argument_values[0])

class Left:

    def __init__(self, turtle):
        self.turtle = turtle

    def invoke(self, procedures, argument_values):
        self.turtle.left(argument_values[0])

class Right:

    def __init__(self, turtle):
        self.turtle = turtle

    def invoke(self, procedures, argument_values):
        self.turtle.right(argument_values[0])

class SetHeading:

    def __init__(self, turtle):
        self.turtle = turtle

    def invoke(self, procedures, argument_values):
        self.turtle.set_heading(argument_values[0])

class SetXY:

    def __init__(self, turtle):
        self.turtle = turtle

    def invoke(self, procedures, argument_values):
        self.turtle.set_x_y(argument_values[0], argument_values[1])

class SetPenColor:

    def __init__(self, turtle):
        self.turtle = turtle

    def invoke(self, procedures, argument_values):
        self.turtle.set_pen_color(argument_values[0], argument_values[1], argument_values[2])

class PenDown:

    def __init__(self, turtle):
        self.turtle = turtle

    def invoke(self, procedures, argument_values):
        self.turtle.pen_down()

class PenUp:

    def __init__(self, turtle):
        self.turtle = turtle

    def invoke(self, procedures, argument_values):
        self.turtle.pen_up()

class Constant:

    def __init__(self, value):
        self.value = value

    def evaluate(self, environment):
        return self.value

class Parameter:

    def __init__(self, name):
        self.name = name

    def evaluate(self, environment):
        return environment[self.name]

class ArithmeticExpression:

    def __init__(self, left, operator, right):
        self.left = left
        self.operator = operator
        self.right = right

    def evaluate(self, environment):
        left_value = self.left.evaluate(environment)
        right_value = self.right.evaluate(environment)
        if self.operator == '+':
            return left_value + right_value
        if self.operator == '-':
            return left_value - right_value
        if self.operator == '*':
            return left_value * right_value
        if self.operator == '/':
            return left_value // right_value
        raise NotImplementedError(self.operator)

class DisjunctionExpression:

    def __init__(self, left, right):
        self.left = left
        self.right = right

    def evaluate(self, environment):
        if self.left.evaluate(environment):
            return True
        return self.right.evaluate(environment)

class ComparisonExpression:

    def __init__(self, left, comparison, right):
        self.left = left
        self.comparison = comparison
        self.right = right

    def evaluate(self, environment):
        left_value = self.left.evaluate(environment)
        right_value = self.right.evaluate(environment)
        if self.comparison == '<':
            return left_value < right_value
        if self.comparison == '<=':
            return left_value <= right_value
        if self.comparison == '=':
            return left_value == right_value
        if self.comparison == '!=':
            return left_value != right_value
        if self.comparison == '>=':
            return left_value >= right_value
        if self.comparison == '>':
            return left_value > right_value
        raise NotImplementedError(self.comparison)

class Logo:

    def __init__(self, turtle):
        self.procedures = {
            'forward': Forward(turtle),
            'back': Back(turtle),
            'left': Left(turtle),
            'right': Right(turtle),
            'setheading': SetHeading(turtle),
            'setxy': SetXY(turtle),
            'setpencolor': SetPenColor(turtle),
            'pendown': PenDown(turtle),
            'penup': PenUp(turtle)
        }

    def execute(self, text):
        parser = Parser(Tokenizer(text))
        statement = parser.parse_top_level_statement()
        while statement is not None:
            statement.execute(self.procedures, {})
            statement = parser.parse_top_level_statement()

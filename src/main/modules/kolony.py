def eyes_open(fb, x, y):
    left_eye_open(fb, x, y)
    right_eye_open(fb, x, y)

def eyes_closed(fb, x, y):
    left_eye_closed(fb, x, y)
    right_eye_closed(fb, x, y)

def left_eye_open(fb, x, y):
    _polyline(fb, x, y, [(7, 11), (12, 17)])
    _polyline(fb, x, y, [(13, 10), (8, 17)])

def right_eye_open(fb, x, y):
    _polyline(fb, x, y, [(17, 6), (22, 13)])
    _polyline(fb, x, y, [(22, 6), (17, 13)])

def left_eye_closed(fb, x, y):
    _polyline(fb, x, y, [(7, 14), (13, 13)])

def right_eye_closed(fb, x, y):
    _polyline(fb, x, y, [(16, 11), (23, 10)])

def mouth_flat(fb, x, y):
    _polyline(fb, x, y, [(7, 22), (18, 20), (26, 20), (23, 21), (22, 25), (20, 21), (15, 22), (14, 27), (12, 23), (7, 22)])

def _polyline(fb, x, y, ps):
    q = None
    for p in ps:
        if not q is None:
            fb.line(x + q[0], y + q[1], x + p[0], y + p[1], 1)
        q = p

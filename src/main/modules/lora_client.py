from lora_states import NOT_JOINED, JOINING, JOINED, SENDING, SENT, RETRY

def _create_modem_state_changed_test(client):
    return lambda : client._has_modem_state_changed()

def _create_modem_state_change_task(client):
    return lambda : client._on_modem_state_change()

def _create_start_join_task(client):
    return lambda : client._start_join()

def _create_start_send_message_task(client):
    return lambda : client._start_send_message()

class LoRaClient:

    # expected methods for modem:
    # modem.send_join()
    # modem.send_message(bytes, confirmed)
    # modem.has_state_changed()
    # modem.get_state()

    def __init__(self, tasks, modem):
        self.tasks = tasks
        self.modem = modem
        self.state = NOT_JOINED
        self.listener = None
        self.queue = []

    def send_text_message(self, text, encoding = 'ASCII', confirmed = False):
        self.send_message(bytes(text, encoding), confirmed)

    def send_message(self, message, confirmed = False):
        self.queue.append((message, confirmed))
        if self.state == NOT_JOINED:
            self._start_join()
        elif self.state == JOINED:
            self._start_send_message()

    def has_pending_message(self):
        return len(self.queue) > 0

    def get_state(self):
        return self.state

    def set_state_change_listener(self, listener):
        self.listener = listener

    def _change_state(self, next_state):
        if self.state != next_state:
            self.state = next_state
            if self.listener is not None:
                try:
                    self.listener(next_state)
                except:
                    pass

    def _start_join(self):
        self.modem.send_join()
        self._change_state(JOINING)
        self._wait_for_modem_state_change()

    def _start_send_message(self):
        message, confirmed = self.queue[0]
        self.modem.send_message(message, confirmed)
        self._change_state(SENDING)
        self._wait_for_modem_state_change()

    def _has_modem_state_changed(self):
        return self.modem.has_state_changed()

    def _on_modem_state_change(self):
        self._change_state(self.modem.get_state())
        if self.state in (JOINING, SENDING):
            self._wait_for_modem_state_change()
        elif self.state in (JOINED, RETRY):
            self._start_send_message_later()
        elif self.state == SENT:
            self.queue.pop(0)
            self._start_send_message_later()
        else:
            self._start_join_later()

    def _wait_for_modem_state_change(self):
        self.tasks.when_then(_create_modem_state_changed_test(self), _create_modem_state_change_task(self))

    def _start_send_message_later(self):
        if self.has_pending_message():
            self.tasks.after(30000, _create_start_send_message_task(self))
        elif self.state == SENT:
            self._change_state(JOINED)

    def _start_join_later(self):
        self.tasks.after(90000, _create_start_join_task(self))

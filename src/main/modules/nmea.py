def _create_available_test(nmea):
    return lambda : nmea._available()

def _create_poll_lines_task(nmea):
    return lambda : nmea._poll_lines()

class NMEA:

    def __init__(self, tasks,  uart):
        self.tasks = tasks
        self.uart = uart
        self.time = None
        self.date = None
        self._clear_position()

    def _clear_position(self):
        self.latitude = None
        self.longitude = None
        self.altitude = None
        self.velocity = None

    def start(self):
        self._poll_lines()

    def get_coordinates(self):
        return (self.latitude, self.longitude, self.altitude)

    def get_velocity(self):
        return self.velocity

    def get_date_time(self):
        if self.date is None or self.time is None:
            return None
        return self.date + 'T' + self.time + 'Z'

    def get_time(self):
        return self.time

    def _available(self):
        return self.uart.any() > 1

    def _poll_lines(self):
        self._read_line()
        self.tasks.when_then(_create_available_test(self), _create_poll_lines_task(self))

    def _read_line(self):
        if self.uart.any() == 0:
            return
        try:
            line = str(self.uart.readline(), 'ASCII')[:-2]
            if not NMEA._check(line):
                return
            words = line.split(',')
            firstword = words[0]
            if firstword == '$GPGGA':
                fix_quality = words[6]
                if fix_quality == '0':   # invalid
                    self._clear_position()
                else:
                    self._set_time(words[1])
                    self._set_latitude(words[2], words[3])
                    self._set_longitude(words[4], words[5])
                    self._set_altitude(words[9])
            elif firstword == '$GPGLL':
                active = words[6]
                if active == 'V':  # void
                    self._clear_position()
                else:
                    self._set_latitude(words[1], words[2])
                    self._set_longitude(words[3], words[4])
                    self._set_time(words[5])
            elif firstword == '$GPVTG':
                self._set_velocity(words[7])
            elif firstword == '$GPZDA':
                self._set_time(words[1])
                self._set_date(words[2], words[3], words[4])
        except:
            pass

    def _set_time(self, time):
        self.time = time[0:2] + ':' + time[2:4] + ':' + time[4:]

    def _set_date(self, day, month, year):
        self.date = year + '-' + month + '-' + day

    def _set_latitude(self, latitude, ns):
        degrees = float(latitude[0:2])
        minutes = float(latitude[2:])
        self.latitude = (degrees + minutes / 60.0) * (1.0 if ns == 'N' else -1.0)

    def _set_longitude(self, longitude, ew):
        degrees = float(longitude[0:3])
        minutes = float(longitude[3:])
        self.longitude = (degrees + minutes / 60.0) * (1.0 if ew == 'E' else -1.0)

    def _set_altitude(self, altitude):
        if len(altitude) > 0:
            self.altitude = float(altitude)

    def _set_velocity(self, velocity):
        if len(velocity) > 0:
            self.velocity = float(velocity)

    @staticmethod
    def _check(line):
        pos = 0
        sum = 0
        for char in line:
            if char == '$':
                if pos != 0:
                    return False
            elif char == '*':
                try:
                    return int(line[pos + 1:], 16) == sum
                except ValueError:
                    return False
            else:
                sum ^= ord(char)
            pos += 1
        return False

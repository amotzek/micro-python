# implement utime on time for use during tests

import time

def sleep_ms(ms):
    time.sleep(float(ms) / 1000.0)

def ticks_ms():
    return int(1000.0 * time.monotonic())

def ticks_add(ticks, delta):
    return ticks + delta

def ticks_diff(ticks1, ticks2):
    return ticks1 - ticks2

import utime
from micropython import const
from framebuf import FrameBuffer, MONO_VLSB
from machine import Pin

_ADDR_MODE = const(0x20)
_COL_ADDR = const(0x21)
_PAGE_ADDR = const(0x22)
_DISP_OFF = const(0xae)
_MULTIPLEX_RATIO = const(0xa8)
_OFFSET = const(0xd3)
_START_LINE_0 = const(0x40)
_SEG_REMAP_0 = const(0xa0)
_COM_OUTPUT_SCAN = const(0xc0)
_COM_PIN_CFG = const(0xda)
_CONTRAST = const(0x81)
_DISABLE_ENTIRE_ON = const(0xa4)
_NORM = const(0xa6)
_CLK_DIV = const(0xd5)
_VCOM_DESEL_LEVEL = const(0xdb)
_PRECHARGE = const(0xd9)
_CHARGE_PUMP = const(0x8d)
_DISP_ON = const(0xaf)

class SSD1306:

    def __init__(self, width, height, i2c, addr = 0x3c, rst = None, external_vcc = False):
        self.width = width
        self.height = height
        self.char_width = 8
        self.char_height = 8
        self.external_vcc = external_vcc
        self.pixels = bytearray((width * height) >> 3)
        self.frame_buffer = FrameBuffer(self.pixels, width, height, MONO_VLSB)
        self.command = bytearray([0x80, 0x00])
        self.data = [b'\x40', self.pixels]
        self.i2c = i2c
        self.addr = addr
        self.rst = None if rst is None else Pin(rst, Pin.OUT)
        self._reset()
        self.off()
        self._setup()

    def on(self):
        self._write_command(_DISP_ON)

    def off(self):
        self._write_command(_DISP_OFF)

    def to_color(self, red, green, blue):
        return 1 if (red > 0 or green > 0 or blue > 0) else 0

    def draw_string(self, x, y, s, color):
        self.frame_buffer.text(s, x, y, color)
        self._show()

    def draw_polyline(self, x, y, points, color):
        last_point = None
        for point in points:
            if last_point is not None:
                self.frame_buffer.line(x + last_point[0], y + last_point[1], x + point[0], y + point[1], color)
            last_point = point
        self._show()

    def scroll_up(self, height):
        self.frame_buffer.scroll(0, -height)
        self.frame_buffer.fill_rect(0, self.height - height, self.width, height, 0)
        self._show()

    def _reset(self):
        if self.rst is not None:
            self.rst.value(0)
            utime.sleep_ms(1)
            self.rst.value(1)
            utime.sleep_ms(50)

    def _setup(self):
        for command in (_MULTIPLEX_RATIO, self.height - 1,
                        _OFFSET, 0x00,
                        _ADDR_MODE, 0x00,  # horizontal
                        _START_LINE_0,
                        _SEG_REMAP_0,
                        _COM_PIN_CFG, 0x02 if self.width == 128 and self.height == 32 else 0x12,
                        _CONTRAST, 0x7f,
                        _DISABLE_ENTIRE_ON,
                        _NORM,
                        _CLK_DIV, 0x80,
                        _VCOM_DESEL_LEVEL, 0x30,  # 0.83 VCC
                        _PRECHARGE, 0x22 if self.external_vcc else 0xf1,
                        _CHARGE_PUMP, 0x10 if self.external_vcc else 0x14):
            self._write_command(command)

    def _show(self):
        x0 = 0
        x1 = self.width - 1
        p0 = 0
        p1 = (self.height >> 3) - 1
        if self.width == 64:
            x0 += 32
            x1 += 32
        self._write_command(_COL_ADDR)
        self._write_command(x0)
        self._write_command(x1)
        self._write_command(_PAGE_ADDR)
        self._write_command(p0)
        self._write_command(p1)
        self._write_data()

    def _write_command(self, command):
        self.command[1] = command
        self.i2c.writeto(self.addr, self.command)

    def _write_data(self):
        self.i2c.writevto(self.addr, self.data)

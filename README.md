# Module und Beispiele für MicroPython

## Cooperative Multitasking

Im Verzeichnis src/main/modules findet sich die Bibliothek
cooperative_multitasking.py. Die daraus erzeugte Datei cooperative_multitasking.mpy
kann im Bereich Downloads heruntergeladen werden. Die Datei ist mit mpy-cross für
MicroPython v1.14 erstellt.

### Beispiel

Das Beispiel verwendet die Klasse Tasks, um die Built-in LED blinken zu lassen.

    from cooperative_multitasking import Tasks
    from machine import Pin

    tasks = Tasks()
    led = Pin(0, Pin.OUT)

    def on():
        pin.value(1)
        tasks.after(1000, off)  # after 1000 milliseconds call off()

    def off():
        pin.value(0)
        tasks.after(1000, on)  # after 1000 milliseconds call on()

    tasks.now(on)  # call on() now

    while True:
        tasks.run()


### Details

cooperativemultitasking ist eine Bibliothek, mit der mehrere Funktionen (fast)
gleichzeitig oder unabhängig voneinander ausgeführt werden können. Um das zu
ermöglichen, verwalten Instanzen der Klasse `Tasks` Listen von Aufgaben.

Aufgaben können mit den Methoden `now`, `after`, `when_then` und `when_for_then`
in die Liste aufgenommen werden:

* Mit `now(f)` wird der Aufruf der Funktion `f` an den Anfang der Liste
  gestellt. Der Aufruf von `f` findet dann sofort statt.
* Mit `after(1000, f)` findet der Aufruf der Funktion `f` erst in 1000
  Millisekunden statt.
* Mit `when_then(g, f)` findet der Aufruf der Funktion `f` statt, sobald die
  Funktion `g` den Wert `True` liefert.
* Mit `when_for_then(g, 300, f)` findet der Aufruf der Funktion `f` statt,
  wenn `g` mindestens 300 Millisekunden lang den Wert `True` geliefert hat.

Mit der Methode `only_one_of` kann man deklarieren, dass nur eine von mehreren
Aufgaben ausgeführt werden soll - die, die als erste aktiv wird.

